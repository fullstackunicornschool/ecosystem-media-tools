import fs from "fs";
import im from "node-imagemagick";
import { createRequire } from "module";
const require = createRequire(import.meta.url);
const thumbSettings = require("../image_settings.json");

export default class Image {
	constructor() {
		this.thumbSettings = thumbSettings;
	}
	folderCheck = (basePath, folderToCheck) => {
		try {
			if (!fs.existsSync(`${basePath}/${folderToCheck}`)) {
				fs.mkdirSync(`${basePath}/${folderToCheck}`);
			}
		} catch (error) {
			throw Error(error);
		}
	};
	thumbs = async (request, response, next) => {
		try {
			const { imgName } = request.body;
			const basePath = process.env.__dirname + "/public";
			const splitter = Object.keys(this.thumbSettings)[0];
			const imgNameSplitted = imgName.split(splitter);
			const srcFullPath = basePath + imgNameSplitted[1];
			const { width } = await this.imageSize(srcFullPath);
			for (const thumbKey in this.thumbSettings) {
				if (width >= this.thumbSettings[thumbKey].width) {
					const dstFullPath = basePath + "/" + imgNameSplitted[0] + thumbKey + imgNameSplitted[1];
					this.folderCheck(basePath + "/" + imgNameSplitted[0], thumbKey);
					await this.resizeImage(srcFullPath, dstFullPath, this.thumbSettings[thumbKey].width);
				}
			}
			return response.status(201).send();
		} catch (error) {
			return response.status(422).send(error);
		}
	};
	cropImage = async (srcPath, dstPath, width, height) => {
		return new Promise((resolve, reject) => {
			im.crop(
				{
					srcPath,
					dstPath,
					width,
					height,
					gravity: "Center",
					quality: 1,
				},
				function (err, stdout) {
					if (err) {
						reject(err);
					}
					resolve(stdout);
				}
			);
		});
	};
	resizeImage = async (source, path_to, width) => {
		return new Promise((resolve, reject) => {
			im.resize(
				{
					srcPath: source,
					dstPath: path_to,
					width: width,
					quality: 1,
					progressive: true,
				},
				function (err, stdout) {
					if (err) {
						reject(err);
					}
					resolve(stdout);
				}
			);
		});
	};
	imageSize = (source) => {
		return new Promise((resolve, reject) => {
			im.identify(["-format", "%wx%hx%b", source], function (err, output) {
				if (err) {
					reject(err);
				}
				console.log("--------->>>>>>", err, output);
				const [width, height, fileSize] = output.split("x");
				resolve({
					width: Number(width),
					height: Number(height),
				});
			});
		});
	};
}
