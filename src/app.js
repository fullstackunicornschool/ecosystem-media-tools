import express from "express";
import routes from "./routes.js";
import bodyParser from "body-parser";
import path from "path";
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
process.env.__dirname = path.dirname(__filename).replace("/src", "");

const app = express();
app.use(bodyParser.json());
app.use("/thumbs", routes);
app.use("/alive", (req,res)=>{return res.status(200).send("ecosystem-media-tools is alive")})

export default app;
