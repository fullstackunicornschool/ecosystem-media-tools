import express from "express";
import Picture from "./resolvers/imagesHandler.js";

const router = express.Router();
const picture = new Picture();

// create image thumbs
router.post("/", picture.thumbs);

export default router;
