FROM node:lts-gallium

WORKDIR /app

RUN apt-get update && apt-get install -y apt-utils openssl

RUN apt-get install -y imagemagick

COPY package.json package.json
COPY yarn.lock yarn.lock

COPY . .

RUN yarn install --production

ENTRYPOINT [ "yarn" ]

CMD [ "run", "start" ]